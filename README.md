# API Crawler - Laravel

## Instruções 
* Clone o projeto
* Rode o comando, na raíz do projeto: composer install
* Rode o comando: php artisan serve  (para inicializar o servidor da aplicação).

## Exemplo de Uso:
### Utilizando o Postman (ou outra ferramenta de sua preferência), aplique as seguintes configurações:  

#### Lista:

* Method: POST
* URL: /api/lista

```json
{
	"tipo_veiculo" : "carro",
    "marca": "volkswagem",
    "modelo": "gol",
    "ano_min": 2000,
    "ano_max": 2020,
    "preco_min": 0,
    "preco_max": 90000,
    "km_min": 0,
    "km_max": 9,
    "zero_usado": "zero",
    "revenda_particular": "revenda"    
}
```

### Detalhes:

* Method: POST
* URL: /api/detalhes  

```json
{
  "link":"https://seminovos.com.br/ford-focus-hatch-glx-2.0-16v-5portas-2012-2013--2596710"
}
```



Meu GitHub: https://www.github.com/bpereiraalmeida7